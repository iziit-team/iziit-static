# Iziit Static

Static content delivery server for [Iziit](https://iziit.org).

licence: [GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.txt)  
contact: [contact@iziit.org](mailto:contact@iziit.org)

## config.json

```json
{
	"port": number,
	"rootDirectory": string
}
```
