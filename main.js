// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Static.

Iziit Static is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Static is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Static.  If not, see <https://www.gnu.org/licenses/>.
*/

const autostrada = require('autostrada');
const cors = require('cors');
const express = require('express');
const Skeletons = require('skeletons');
const fileExists = require('file-exists').sync;

function loadConfig() {
	if (!fileExists('./config.json')) {
		console.error('config.json not found');
		process.exit(1);
	}
	const config = require('./config.json');
	const valid = new Skeletons({
		port: Skeletons.Number({
			min: 0,
		}),
		rootDirectory: Skeletons.String(),
	}).validate(config).valid;
	if (valid) {
		return config;
	} else {
		console.error('wrong format in config.json');
		process.exit(1);
	}
}

function main() {
	const config = loadConfig();
	const app = express();
	app.use(cors());
	app.use(autostrada({
		wwwDirectory: config.rootDirectory,
	}));
	app.listen(config.port);
	console.log(`Iziit Static running on port ${config.port}`);
}

main();
